from __future__ import division
import numpy as np
import math
import datetime

nx = 30
ny = 30
dx = 0.1
dy = 0.1
ddx = 1.0/dx
ddy = 1.0/dy
ddx2 = (1.0/(dx**2))
ddy2 = (1.0/(dy**2))
dt = 0.01
Re = 1000 # Re>70 due to the method to compute difference.
speed = 1
u = np.zeros((nx+2, ny+1))  
v = np.zeros((nx+1, ny+2))
p = np.zeros((nx+2, ny+2))  #with dummy mesh
phi = np.zeros((nx+2, ny+2))
divup = np.zeros((nx+2, ny+2))
t = 4000

# default condition
#u[:,ny] = 1

# b.c.
#phi[1][1] = 0
#phi[:,ny+1] = phi[:,ny]
#phi[:,0] = phi[:,1]
#phi[0,:] = phi[1,:]
#phi[nx+1,:] = phi[nx,:]

phi[1][1] = 0
phi[0,1:ny] = phi[1,1:ny]
phi[nx+1,1:ny] = phi[nx,1:ny]
phi[1:nx,0] = phi[1:nx,1]
phi[1:nx,ny+1] = phi[1:nx,ny]
u[:,0] = 0
u[:,nx] = 0
u[0,:] = 2 - u[1,:]
u[nx+1,:] = -u[nx,:]
v[0,:] = 0
v[nx,:] = 0
v[:,0] = -v[:,1]
v[:,ny+1] = -v[:,ny]

for value in range(t):
    times = 400  #  repeat times to solve possion eq
    for i in range(1, nx-1): 
    	for j in range(1, ny):
            #Pre-Calculate for uP
            cnvU = (1.0/dx)*( ((u[i+1][j]+u[i][j])/2.0)**2.0 -  ((u[i][j]+u[i-1][j])/2.0)**2.0 ) + (1.0/dy)*( ((u[i][j+1]+u[i][j])/2.0) * ((v[i+1][j]+v[i][j])/2.0) - ((u[i][j-1]+u[i][j])/2.0) * ((v[i+1][j-1]+v[i][j-1])/2.0))
            #cnvU = ddx * ((u[i+1][j]+u[i][j])**2-(u[i-1][j]+u[i][j])**2) / 4.0 + ddy *((u[i][j+1]+u[i][j]) * (v[i+1][j]+v[i][j])-(u[i][j]+u[i][j-1])*(v[i][j-1]+v[i+1][j-1]))/4.0
            difU = ((u[i-1][j]-2.0*u[i][j]+u[i+1][j])/(dx**2.0))+((u[i][j-1]-2.0*u[i][j]+u[i][j+1])/(dy**2.0))
            u[i][j] = u[i][j] + dt * (-((p[i+1][j]-p[i][j])/dx)-cnvU + (1.0/Re)*difU)
    u[:,0] = 0
    u[:,nx] = 0
    u[0,:] = 2 - u[1,:]
    u[nx+1,:] = -u[nx,:]
    
    for i in range(1, nx):  
    	for j in range(1, ny-1):
            #Pre-Calculate for vP
            cnvV = (1.0/dy)*(((v[i][j]+v[i][j+1])/2.0)**2.0-((v[i][j-1]+v[i][j])/2.0)**2.0) + (1.0/dx)*( ((u[i][j]+u[i][j+1])/2.0) * ((v[i][j]+v[i+1][j])/2.0) - ((u[i-1][j]+u[i-1][j+1])/2.0)*((v[i-1][j]+v[i][j])/2.0))
            #cnvV = ddx*((u[i][j+1]+u[i][j])*(v[i+1][j]+v[i][j])-((u[i-1][j+1]+u[i-1][j])*(v[i-1][j]+v[i][j])))/4.0 + ddy*((v[i][j+1]+v[i][j])**2-(v[i][j]+v[i][j-1])**2)/4.0
            difV = ((v[i-1][j]-2.0*v[i][j]+v[i+1][j])/(dx**2.0))+((v[i][j-1]-2.0*v[i][j]+v[i][j+1])/(dy**2.0))
            v[i][j] = v[i][j] + dt * (-((p[i][j+1]-p[i][j])/dy)-cnvV + (1.0/Re)*difV)
    v[0,:] = 0
    v[nx,:] = 0
    v[:,0] = -v[:,1]
    v[:,ny+1] = -v[:,ny]

    for ia in range(1,nx):
        for ja in range(1,ny):# change:from 1 --> from 2
            divup[ia][ja] = (1.0/dx)*(u[ia][ja]-u[ia-1][ja])+(1.0/dy)*(v[ia][ja]-v[ia][ja-1])
            #div = div + divup[i][j]**2

    phi[:,:] = 0 #phi needs to be initialized
    #solve poisson eq of phi with jacobi method below
    for time in range(times):
        for ii in range(1,nx+1):
            for jj in range(1,ny+1):
                rhs = (1.0/dt)*divup[ii][jj]
                resid = ddx2 * (phi[ii-1][jj] - 2.0*phi[ii][jj] + phi[ii+1][jj]) + ddy2 * (phi[ii][jj-1] - 2.0*phi[ii][jj] + phi[ii][jj+1]) - rhs
                dphi = 1.7 * resid/(2.0*(ddx2 + ddy2))
                phi[ii][jj] = phi[ii][jj] + dphi


    phi[1][1] = 0
    phi[0,1:ny] = phi[1,1:ny]
    phi[nx+1,1:ny] = phi[nx,1:ny]
    phi[1:nx,0] = phi[1:nx,1]
    phi[1:nx,ny+1] = phi[1:nx,ny]
    p = p + phi

    #u = u - np.diff(phi)*ddx*dt
    #v = v - np.diff(phi,1,0)*ddy*dt
    
    p[:,ny+1] = p[:,ny]
    p[:,0] = p[:,1]
    p[0,:] = p[1,:]
    p[nx+1,:] = p[nx,:]


    print "when t={}".format(value),divup[1][1]

#
"""
    if value % 100 == 0:
        with open("d:\kyu{}.vtk".format(value),"a+") as vtkFile:
            xy = (nx+2)*(ny+2)
            vtkFile.write("# vtk DataFile Version 2.0\n")
            vtkFile.write("result in time {}\n".format(value))
            vtkFile.write("ASCII\n")
            vtkFile.write("DATASET STRUCTURED_GRID\n")
            vtkFile.write("DIMENSIONS {} {} 1\n".format(nx+2,ny+2))
            vtkFile.write("POINTS {} int\n".format(xy))
            for m in range(nx+2):
                for j in range(ny+2):
                    vtkFile.write("{} {} 0\n".format(m,j))
            vtkFile.write("POINT_DATA {}\n".format(xy))
            vtkFile.write("SCALARS Pressure float\n")
            vtkFile.write("LOOKUP_TABLE default\n")
            for i1 in range(xy):
                vtkFile.write("{}\n".format(p.flat[i1]))
            vtkFile.write("SCALARS phi float\n")
            vtkFile.write("LOOKUP_TABLE default\n")
            for i2 in range(xy):
                vtkFile.write("{}\n".format(phi.flat[i2]))
            vtkFile.write("SCALARS VelocityMagnitude float\n")
            vtkFile.write("LOOKUP_TABLE default\n")
            for i3 in range(xy):
                vtkFile.write("{}\n".format(math.sqrt(v.flat[i3]**2+u.flat[i3]**2)))
            vtkFile.write("VECTORS velocity float\n")
            for i4 in range(xy):
                vtkFile.write("{} {} 0\n".format(u.flat[i4],v.flat[i4]))
            vtkFile.write("VECTORS velocityP float\n")
            #for i4 in range(xy):
                #vtkFile.write("{} {} 0\n".format(uk.flat[i4],vk.flat[i4]))
        print "draw {} @ ".format(value),datetime.datetime.now()
    """